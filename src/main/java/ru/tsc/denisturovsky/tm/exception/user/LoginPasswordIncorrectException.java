package ru.tsc.denisturovsky.tm.exception.user;

public final class LoginPasswordIncorrectException extends AbstractUserException {

    public LoginPasswordIncorrectException() {
        super("Error! Incorrect login or password entered. Please try again...");
    }

}
