package ru.tsc.denisturovsky.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.denisturovsky.tm.api.repository.ICommandRepository;
import ru.tsc.denisturovsky.tm.api.repository.IProjectRepository;
import ru.tsc.denisturovsky.tm.api.repository.ITaskRepository;
import ru.tsc.denisturovsky.tm.api.repository.IUserRepository;
import ru.tsc.denisturovsky.tm.api.service.*;
import ru.tsc.denisturovsky.tm.command.AbstractCommand;
import ru.tsc.denisturovsky.tm.command.data.AbstractDataCommand;
import ru.tsc.denisturovsky.tm.command.data.DataBase64LoadCommand;
import ru.tsc.denisturovsky.tm.command.data.DataBinaryLoadCommand;
import ru.tsc.denisturovsky.tm.command.system.ApplicationExitCommand;
import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.denisturovsky.tm.exception.system.CommandNotSupportedException;
import ru.tsc.denisturovsky.tm.model.Project;
import ru.tsc.denisturovsky.tm.model.Task;
import ru.tsc.denisturovsky.tm.model.User;
import ru.tsc.denisturovsky.tm.repository.CommandRepository;
import ru.tsc.denisturovsky.tm.repository.ProjectRepository;
import ru.tsc.denisturovsky.tm.repository.TaskRepository;
import ru.tsc.denisturovsky.tm.repository.UserRepository;
import ru.tsc.denisturovsky.tm.service.*;
import ru.tsc.denisturovsky.tm.util.DateUtil;
import ru.tsc.denisturovsky.tm.util.SystemUtil;
import ru.tsc.denisturovsky.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.tsc.denisturovsky.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @NotNull
    private final Backup backup = new Backup(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initBackup() {
        backup.init();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initData() {
        final boolean checkBinary = Files.exists(Paths.get(AbstractDataCommand.FILE_BINARY));
        if (checkBinary) {
            processCommand(DataBinaryLoadCommand.NAME, false);
            return;
        }
        final boolean checkBase64 = Files.exists(Paths.get(AbstractDataCommand.FILE_BASE64));
        if (checkBase64) processCommand(DataBase64LoadCommand.NAME, false);
    }

    private void initDemoData() {
        @NotNull final User user = userService.create("user", "user", "user@user.ru");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);

        taskService.add(
                user.getId(),
                new Task(
                        "TASK TEST",
                        Status.IN_PROGRESS,
                        DateUtil.toDate("10.05.2018"),
                        DateUtil.toDate("10.05.2099")
                )
        );
        taskService.add(
                user.getId(),
                new Task(
                        "TASK BETA",
                        Status.NOT_STARTED,
                        DateUtil.toDate("05.11.2020"),
                        DateUtil.toDate("05.11.2099")
                )
        );
        taskService.add(
                user.getId(),
                new Task(
                        "TASK BEST",
                        Status.IN_PROGRESS,
                        DateUtil.toDate("15.10.2019"),
                        DateUtil.toDate("15.10.2099")
                )
        );
        taskService.add(
                user.getId(),
                new Task(
                        "TASK MEGA",
                        Status.COMPLETED,
                        DateUtil.toDate("07.03.2021"),
                        DateUtil.toDate("07.03.2099")
                )
        );

        projectService.add(
                user.getId(),
                new Project(
                        "PROJECT DEMO",
                        Status.IN_PROGRESS,
                        DateUtil.toDate("07.05.2018"),
                        DateUtil.toDate("07.05.2099")
                )
        );
        projectService.add(
                user.getId(),
                new Project(
                        "PROJECT TEST",
                        Status.NOT_STARTED,
                        DateUtil.toDate("03.11.2020"),
                        DateUtil.toDate("03.11.2099")
                )
        );
        projectService.add(
                user.getId(),
                new Project(
                        "PROJECT BEST",
                        Status.IN_PROGRESS,
                        DateUtil.toDate("05.10.2019"),
                        DateUtil.toDate("05.10.2099")
                )
        );
        projectService.add(
                user.getId(),
                new Project(
                        "PROJECT MEGA",
                        Status.COMPLETED,
                        DateUtil.toDate("04.03.2021"),
                        DateUtil.toDate("04.03.2099")
                )
        );
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(
                new Thread(() -> loggerService.info("** TASK MANAGER IS SHUTTING DOWN **"))
        );
    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args)) new ApplicationExitCommand().execute();
        initPID();
        initLogger();
        initDemoData();
        initData();
        initBackup();
        while (true) {
            try {
                System.out.println("Enter command:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                loggerService.command(command);
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    public void processCommand(@Nullable final String command) {
        processCommand(command, true);
    }

    public void processCommand(@Nullable final String command, boolean checkRoles) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        if (checkRoles) authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

}
